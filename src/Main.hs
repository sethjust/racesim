module Main (module Main) where

import RaceSim

import Haste
--import Haste.DOM
--import Haste.Events
import Haste.Foreign

main :: IO ()
main = chart result
  where
    carA = Car "test car" 80 250 7.9377 1500 0.725 -- 80 kW, 250 Nm, 7.9 gears, 3300 lbs, 0.725 m^2 drag area
    carB = Car "leaf-swapped '83 civic" 80 250 7.9377 (780 - 136 + 425) 0.65 -- total guess on curb weight, informed guesses on weights, drag area
    tr = Track "1/4 mi strip" 402.3
    result = map (dragRace tr) [carA, carB]

    chart :: [DragResult] -> IO ()
    chart = ffi ( toJSString
      "(function (data) {\
      \  console.log(data);\
      \  return c3.generate({\
      \    bindto:'#chart',\
      \    data: data,\
      \    axis: {\
      \      x: {label: {text: 'Time (s)', position: 'outer-right'}},\
      \      y: {label: 'Distance (m)'},\
      \      y2: {show: true, label: {text: 'Speed (m/s)', position: 'outer-top'}}\
      \    }\
      \  });\
      \})"
      ) . toData

    toData :: [DragResult] -> JSAny
    toData rs = toObject [
            (toJSString "columns", toAny columns),
            (toJSString "xs", toAny xs),
            (toJSString "axes", axes),
            (toJSString "names", names)
          ]
      where
        results :: [(Int, DragResult)]
        results = zip [0..] rs
        nm s = map (\(i, _) -> s ++ show i)
        nts = nm "time" results
        nds = nm "distance" results
        nss = nm "speed" results
        names = toObject $ concatMap names' results
        names' (i, res) = [
            (toJSString (nds !! i), toAny (name (dragResultCar res) ++ " Position")),
            (toJSString (nss !! i), toAny (name (dragResultCar res) ++ " Speed"))
          ]
        xs = toObject $ toX nts nds ++ toX nts nss
        toX = zipWith toX'
        toX' x y = (toJSString y, toAny x)
        axes = toObject $ map (toAxis "y") nds ++ map (toAxis "y2") nss
        toAxis a s = (toJSString s, toAny a)
        columns = concatMap toCols results
        toCols (i, res) = [
            toAny (nts !! i) : tCol res,
            toAny (nds !! i) : dCol res,
            toAny (nss !! i) : sCol res
          ]
        tCol = col stateTime dragResultTime
        dCol = col stateDist (trackLength . dragResultTrack)
        sCol = col stateSpeed dragResultSpeed
        col f b res = map toAny $ map f (history res) ++ [b res]
