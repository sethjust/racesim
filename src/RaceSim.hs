module RaceSim (module RaceSim) where

import Text.Printf (printf)
import Data.List (intercalate)

class Named a where
  name :: a -> String

data Car = Car {
  carName :: String,
  -- we assume that the car is always "flat out",
  -- with the given max torque/power.
  maxPower :: Double,
  maxTorque :: Double, -- engine torque, subject to gear ratio
  ratio :: Double, -- todo: speed-based ratio changes
  mass :: Double,
  dragArea :: Double
} deriving (Eq)

instance Named Car where
  name = carName

data Track = Track {
  trackName :: String,
  trackLength :: Double
} deriving (Eq, Show)

instance Named Track where
  name = trackName

data DragState = DragState {
  stateTrack :: Track,
  stateCar :: Car,
  stateTime :: Double,
  stateDist :: Double,
  stateSpeed :: Double
}

data DragResult = DragResult {
  dragResultTrack :: Track,
  dragResultCar :: Car,
  dragResultTime :: Double,
  dragResultSpeed :: Double,
  history :: [DragState]
}

instance Show DragResult where
  show res@(DragResult tr car t s _) =
    intercalate "\n" $ map (toPosBar res) (doRace tr car)
    ++ ["------------", "time: " ++ show t ++ " sec", "speed: " ++ show s ++ " m/s", "track: " ++ show tr, "car: " ++ show car]

toPosBar :: DragResult -> DragState -> String
toPosBar res@(DragResult _ car _ _ _) (DragState _ _ time dist speed) = let
    rps = motorRps car speed
    rpm = rps * 60
    t = torque car speed
    p = torqueToPower t rps
    a = accel car speed
    n = floor $ dist / (trackLength . dragResultTrack) res * 40
    bar = concat (replicate n "*")
  in printf "time=%7.3f dist=%7.3f speed=%7.3f rpm=%7.1f torque=%7.3f power=%7.3f drag=%7.2f accel=%7.3f  %s" time dist speed rpm t p (drag car speed) a bar

instance Show Car where
  show car@(Car nm p t rt m dA) =
      printf "\"%s\" -- maxPower=%f maxTorque=%f rt=%f dA=%f m=%f\n%s\n" nm p t rt dA m dynoChart
    where
      dynoLine s =
        printf "%6.2f m/s %7.1f rpm %6.2f Nm %6.2f Kw  |%s" s (rps * 60) tq pw bar
        where
          tq = torque car s
          nt = floor $ tq / t * 80
          rps = motorRps car s
          pw = torqueToPower tq rps
          np = floor $ pw / p * 80
          bar = let
              a = concat $ replicate 80 " "
              b = take np a ++ ['*'] ++ drop (np + 1) a
            in take nt b ++ ['#'] ++ drop (nt + 1) b
      dynoChart = intercalate "\n" $ map dynoLine [0,2.5..50] -- m/s

dragRace :: Track -> Car -> DragResult
dragRace tr car = finish $ doRace tr car

doRace :: Track -> Car -> [DragState]
doRace tr car = doRace' $ DragState tr car 0 0 0
doRace' :: DragState -> [DragState]
doRace' = takeWhile (not . finished) . iterate stepRace

finished :: DragState -> Bool
finished (DragState (Track _ l) _ _ d _) = d >= l

stepRace :: DragState -> DragState
stepRace st = stepState st $ step st

finish :: [DragState] -> DragResult
finish h = finish' h $ last h
  where
    finish' h' st@(DragState tr _ _ d _) = finish'' h' $ stepState st $ stepDist (trackLength tr - d) st

finish'' :: [DragState] -> DragState -> DragResult
finish'' h (DragState tr c t _ ss) = DragResult tr c t ss h -- assumes the state is at the right distance

-- | takes a (delta time, delta dist, final speed) tuple and applies it to an existing state
stepState :: DragState -> (Double, Double, Double) -> DragState
stepState (DragState tr c t d ss) (dt, dd, fs) | ss <= 0 && accel c ss <= 0  = let infinity = 1/0 in DragState tr c infinity infinity 0
                                               | otherwise                   = DragState tr c (t + dt) (d + dd) fs

step :: DragState -> (Double, Double, Double) -- time, dist, speed
step = stepTimeRk 0.25 -- delta t for each step
--step = stepTime 0.025 -- delta t for each step
--step = stepDist 0.1 -- delta d for each step

r :: Car -> Double
--r car = 1/6 -- m
r _ = 0.316 -- radius of 205/55R16

-- | compute rps from speed, r, ratio
-- | m/s / m/rot == rot/s
-- | rot/s * ratio = motorRps
-- | m/s / m = s^-1 = Hz
motorRps :: Car -> Double -> Double
motorRps c s = ratio c * s / (2 * pi * r c)

-- | for a car at a given motor rps
-- | compute the torque for a given power
-- | torque * rps = power
-- | torque = power / rps
-- | 1 N m * 1 rev = 2 pi J
-- | 1 rev = 2 pi rad
-- ! 1 N m = 1 J / rad
-- | 1 W = 1 J / s
-- | 1 kW s = 1000 N m
powerToTorque :: Car -> Double -> Double
powerToTorque c rps = 1000 * maxPower c / (rps * 2 * pi)

torqueToPower :: Double -> Double -> Double
torqueToPower t rps = t * rps * 2 * pi / 1000

loss :: Car -> Double -> Double
loss _ t = 0.14 * t -- TODO: speed dependent torque loss

torque :: Car -> Double -> Double
torque c s = let
    m = maxTorque c
    pt = powerToTorque c $ motorRps c s
    t = min m pt
  in t - loss c t

-- | car -> speed -> coefficient of rolling resitance
-- | TODO: depend on track surface
rrc :: Car -> Double -> Double
rrc _ _ = 0.012

rr :: Car -> Double -> Double
rr car speed = rrc car speed * 9.8066 * mass car -- unit is N; 1 kg == 9.8066 N under gravity

-- torque required to overcome rolling resistance
-- this is rr * linear speed / rotational speed
-- we assume (for now) 0 slip, so the length is the wheel radius
rrt :: Car -> Double -> Double
rrt car speed = rr car speed * r car -- units are N m

-- helper for subtracting rolling res. from torque
_nt :: Double -> Double -> Double
_nt t res | t > res  = t - res
        | otherwise = 0 -- fall-through to handle e.g. NaN
        -- | res >= t = 0--error "rolling reses. exceeds torque"

-- torque (after rolling resistance) that goes to accelerate
-- is divided by the length of the lever arm to give force
force :: Car -> Double -> Double
force c s = _nt (torque c s * ratio c) (rrt c s) / r c -- N m / m = N

density :: Double
density = 1.225 -- kg/m^3

drag :: Car -> Double -> Double
drag c s = 0.5 * density * dragArea c * s * s -- kg m^2 m^2 / m^3 s^2 = kg m / s^2 = N

accel :: Car -> Double -> Double
accel c s = (force c s - drag c s) / mass c -- units are N m / kg m == m s^-2

-- | direct implementation of euler's method
stepTime :: Double -> DragState -> (Double, Double, Double)
stepTime dt (DragState _ c _ _ ss) = (dt, dd, fs)
  where 
    a = accel c ss
    fs = ss + dt * a
    dd = ss * dt + 1/2 * a * dt * dt

-- | alternative non-adaptive solver
-- | returns (time step, distance step, final speed)
stepTimeRk :: Double -> DragState -> (Double, Double, Double)
stepTimeRk dt (DragState _ c t0 _ s0) =
  let
    -- y' = f(t, y)
    -- accel = f(t, speed)
    -- fn _ speed = accel c speed
    fn _ = accel c
    -- rk4 impl
    rk f (t, y) h = (t + h, y + h * (k1 + 2.0 * k2 + 2.0 * k3 + k4) / 6.0)
      where
        k1 = f t y
        k2 = f (t + 0.5 * h) (y + 0.5 * h * k1)
        k3 = f (t + 0.5 * h) (y + 0.5 * h * k2)
        k4 = f (t +       h) (y +       h * k3)
    -- invoke rk4 to compute final speed
    (_, fs) = rk fn (t0, s0) dt
    -- assuming constant accel
    --dd = s0 * dt + 1/2 * (fn t0 s0) * dt * dt
    -- using average speed
    --dd = (s0 + fs) * dt / 2
    -- we want to use RK to compute distance from speed
    -- speed = f(t, pos)
    -- assume linear change in speed (it's probably overkill to use RK for this)
    fn' t _ = s0 + ((t - t0) / dt) * (fs - s0)
    (_, dd) = rk fn' (t0, 0) dt
  in
    (dt, dd, fs)

stepDist :: Double -> DragState -> (Double, Double, Double)
stepDist dd (DragState _ c _ _ ss) = (dt, dd, fs)
  where
    a = accel c ss
    -- equation for constant acceleration
    -- pos = pos_init + speed_init * time + 1/2 * accell * time^2
    -- if pos_init = -dd, then the point we want is at pos = 0
    -- solving for time we use the quadratic equation when pos = 0
    -- x = (-b +/- sqrt(b^2 - 4ac))/2a
    -- time = (-speed_init +/- sqrt(speed_init^2 - 2 * pos_init * accell)) / accell
    -- time = (-ss +/- sqrt(ss^2 + 2 * dd * a) / a
    -- with t = 0, the time variable gives dt directly, which is what we want
    -- straightforward translation, assuming positive speed to choose which root (note: we must special case a = 0)
    -- _dt a | a == 0     = dd / ss
    --       | otherwise  = (-ss + sqrt(ss^2 + 2 * dd * a)) / a
    -- however, this suffers loss of precision when ss >> a, causing rounding to zero in some cases!
    -- thus we apply the so-called Citardauq Formula: x = 2c / (-b -/+ sqrt(b^2 - 4ac))
    -- note that this gracefully handles a = 0
    -- to get the root we want, switch the sign of the +/-
    -- -2 * dd / (-ss - sqrt(ss^2 + 2 * dd * a))
    -- finally, cancel signs
    dt = 2 * dd / (ss + sqrt(ss * ss + 2 * dd * a))
    fs = ss + dt * a
