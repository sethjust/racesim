module Main (module Main) where

import RaceSim

import Control.Monad.Reader -- has Monad impl for ->

import Data.Function (on)
import Text.Printf (printf)

import Test.Hspec
import Test.QuickCheck

main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "drag race" $ do
  it "finishes at a positive time" $ property $
    (>0) . dragResultTime
  it "finishes with the same car it started with" $ property $
    \t c -> dragResultCar (dragRace t c) == c
  it "finishes on the track it started on" $ property $
    \t c -> dragResultTrack (dragRace t c) == t
  it "takes longer with less torque" $ property $
    let
      race :: Gen (DragResult, DragResult)
      race = do
        tr <- arbitrary
        carA <- arbitrary
        bT <- genTorque
        let
          carB = Car ("clone of " ++ name carA) (maxPower carA) bT (ratio carA) (mass carA) (dragArea carA)
          toRes = dragRace tr
        return (toRes carA, toRes carB)
    in
      forAll race $
        \(a, b) -> let
          gT = (>) `on` (maxTorque . dragResultCar)
          lT = (<=) `on` dragResultTime -- allow equal times to handle extreme or infinite cases
        in gT a b ==> lT a b
  it "takes longer with less power" $ property $
    let
      race :: Gen (DragResult, DragResult)
      race = do
        tr <- arbitrary
        carA <- arbitrary
        bP <- genPower
        let
          carB = Car ("clone of " ++ name carA) bP (maxTorque carA) (ratio carA) (mass carA) (dragArea carA)
          toRes = dragRace tr
        return (toRes carA, toRes carB)
    in
      forAll race $
        \(a, b) -> let
          gP = (>) `on` (maxPower . dragResultCar)
          lT = (<=) `on` dragResultTime -- allow equal times if torque is extreme
        in gP a b ==> lT a b
  it "takes longer with more weight" $ property $
    let
      race :: Gen (DragResult, DragResult)
      race = do
        tr <- arbitrary
        carA <- arbitrary
        bM <- genMass
        let
          carB = Car ("clone of " ++ name carA) (maxPower carA) (maxTorque carA) (ratio carA) bM (dragArea carA)
          toRes = dragRace tr
        return (toRes carA, toRes carB)
    in
      forAll race $
        \(a, b) -> let
          gP = (<) `on` (mass . dragResultCar)
          lT = (<=) `on` dragResultTime -- allow equality to pass in case of infinity
        in gP a b ==> lT a b
  it "takes longer with more drag" $ property $
    let
      race :: Gen (DragResult, DragResult)
      race = do
        tr <- arbitrary
        carA <- arbitrary
        bDa <- genDragArea
        let
          carB = Car ("clone of " ++ name carA) (maxPower carA) (maxTorque carA) (ratio carA) (mass carA) bDa
          toRes = dragRace tr
        return (toRes carA, toRes carB)
    in
      forAll race $
        \(a, b) -> let
          lA = (<) `on` (dragArea . dragResultCar)
          lT = (<=) `on` dragResultTime -- allow equality in extreme cases
        in lA a b ==> lT a b
  it "correctly caps torque to max power" $ property $
      let
        c = Car "test car" 80 250 7.9377 1500 0.725
        rpm = 12000
        rps = rpm / 60
        tq = powerToTorque c rps
      in
        counterexample (show (c,rpm,rps,tq)) $ between 60 65 tq
  it "takes infinite time without acceleration" $ property $
    \tr ->
      let 
        infinity = 1 / 0
        car = Car "test car" 1 0 1 1 1
      in
        counterexample (show car) $ infinity == dragResultTime (dragRace tr car)
  it "takes time to cover the final distance of a race" $ property $
    let
      tr = Track "test strip" 1770.9106214738167
      car = Car "test car" 55.138219512422886 93.32181381009993 9.52514808634314 274.82551466077507 2.9545737544954376
      sts = doRace tr car
      st = last sts
      t = stateTime st
      a = accel (stateCar st) (stateSpeed st)
      stp = stepDist (trackLength tr - stateDist st) st
      st_ = stepState st stp
      ft = stateTime st_
      res = finish sts
    in
      counterexample (printf "start at %.4f (\n%s\n)\n%s\naccel=%f\nend at %.3f (\n%s\n)\n%s" t (toPosBar res st) (show stp) a ft (toPosBar res st_) (show res)) $ ft > t && ft == dragResultTime res
  it "gives reasonable numbers" $ property $
    -- first, setup some "known" values
    -- realistic electric car on a 1/4 mi
    let
       car = Car "test car" 80 250 7.9377 1500 0.725 -- 80 kW, 250 Nm, 7.9 gears, 3300 lbs, 0.725 m^2 drag area
       tr = Track "1/4 mi strip" 402.3
       res = dragRace tr car
    in
      counterexample (show res) $ -- print the result in case of failure
        -- these numbers come from a rule-of-thumb
        -- drag ET calculator
        between 15 25 (dragResultTime res) -- predicted ~20 sec
        &&
        between 25 45 (dragResultSpeed res) -- predicted ~35 m/s (~78 mph)
        -- && False -- fail to force debugging
    -- this test case passes, but we still don't model
    --   - traction

between :: Ord a => a -> a -> a -> Bool
between a b = liftM2 (&&) (>a) (<b)

--headToHead :: Gen (DragResult, DragResult)
--headToHead = do
--  tr <- arbitrary
--  (carA, carB) <- arbitrary
--  let r = dragRace tr in
--    return (r carA, r carB)

instance Arbitrary DragResult where
  arbitrary = do
    tr <- arbitrary
    car <- arbitrary
    return $ dragRace tr car

instance Arbitrary Track where
  arbitrary = do
    len <- genPositive `suchThat` (>20)
    return $ Track (printf "%.3fm strip" len) len

genPower :: Gen Double
genPower = genPositive `suchThat` between 10 500 -- kW

genTorque :: Gen Double
genTorque = genPositive `suchThat` between 10 1500 -- Nm

genMass :: Gen Double
genMass = genPositive `suchThat` between 200 2500 -- kg

genDragArea :: Gen Double
genDragArea = genPositive `suchThat` between 0.1 4

instance Arbitrary Car where
  arbitrary = do
    nm <- genName
    power <- genPower
    tq <- genTorque
    rt <- genPositive `suchThat` between 0.1 10 -- rt
    m <- genMass
    da <- genDragArea
    return $ Car nm power tq rt m da

genName :: Gen String
genName = fmap (show . getPositive) (arbitrary :: Gen (Positive Int))

genPositive :: Gen Double
genPositive = fmap getPositive (arbitrary :: Gen (Positive Double))
